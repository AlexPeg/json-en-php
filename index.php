<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/card.min.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<div class="ui three stackable cards">



<?php
use League\Csv\Reader;

require 'vendor/autoload.php';

$myVar = Reader::createFromPath('cs_figures.csv', 'r');
$myVar->setHeaderOffset(0); //set the CSV header offset


foreach ($myVar as $value) {
    ?>

<div class="card">
    <div class="image">
      <img src="<?php echo $value['picture']; ?>">
    </div>
    <div class="content">
      <div class="header"><?php echo $value['name']; ?></div>
      <div class="meta">
        <a>Friends</a>
      </div>
      <div class="description">
      <?php echo $value['title']; ?>
      </div>
    </div>
    <div class="extra content">
      <span class="right floated">
      <?php echo $value['birthyear']; ?>
      </span>
      <span>
        <i class="user icon"></i>
        <?php echo $value['role']; ?>
       
      </span>
    </div>
  </div>

  <?php  
}



?>
</div>
</div>
</div>
</body>
</html>




<?PHP
/* function read($csv){
    $file = fopen($csv, 'r');
    while (!feof($file) ) {
        $line[] = fgetcsv($file, 1024);
    }
    fclose($file);
    return $line;
}
// Définir le chemin d'accès au fichier CSV
$csv = 'cs_figures.csv';
$csv = read($csv);
echo '<pre>';
print_r($csv);
echo '</pre>'; */
